
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1;

namespace Tests
{
    public class Tests
    {

        StringCalculator stringCalculator = new StringCalculator();

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void TestZero()
        {
            string str = "";
            Assert.AreEqual(0, stringCalculator.Calculate(str));
        }

        [Test]
        public void TestSingleNumber()
        {
            string str = "13";
            Assert.AreEqual(13, stringCalculator.Calculate(str));
        }

        [Test]
        public void TestTwoNumersSum()
        {
            string str = "2.3";
            Assert.AreEqual(5, stringCalculator.Calculate(str));
        }

        [Test]
        public void TestTwoNumersSumNewLine()
        {
            string str = "8\n7";
            Assert.AreEqual(15, stringCalculator.Calculate(str));
        }


        [Test]
        public void TestManyNumersSum()
        {
            string str = "8\n7.2";
            Assert.AreEqual(17, stringCalculator.Calculate(str));
        }

        [Test]
        public void TestNegativeNumber()
        {
            string str1 = "-15";
            string str2 = "15.8.-7";
            Assert.Throws<Exception>(() => stringCalculator.Calculate(str1));
            Assert.Throws<Exception>(() => stringCalculator.Calculate(str2));
        }

        [Test]
        public void TestIfgreater()
        {
            string str1 = "10000";
            string str2 = "12.8.2\n80000";
            Assert.AreEqual(0, stringCalculator.Calculate(str1));
            Assert.AreEqual(22, stringCalculator.Calculate(str2));
        }

        [Test]
        public void TestDelimiter()
        {
            string str1 = "//#10#10";
            string str2 = "//T1000";
            string str3 = "//?10.0\n10?20";
            Assert.AreEqual(20, stringCalculator.Calculate(str1));
            Assert.AreEqual(1000, stringCalculator.Calculate(str2));
            Assert.AreEqual(40, stringCalculator.Calculate(str3));
        }
    }
}