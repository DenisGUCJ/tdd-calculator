﻿using System;

namespace ConsoleApp1
{ 
    public class StringCalculator
    {
        public int Calculate(string input)
        {
            int value = 0;
            int incrementer = 0;


            if (input == null)
            {
                return value;
            }

            else
            {

                if (Int32.TryParse(input, out value))
                {
                    if (value < 0)
                    {
                        throw new Exception();
                    }

                    Ifgreater(value, out value);
                    return value;
                }

                else
                {

                    string temp = "";
                    char delimiter = ' ';
                    int counter = 0;

                    for (int i = 0; i < input.Length; i++)
                    {
                        if (input[i] == '/')
                        {
                            counter++;
                        }
                        else if (input[i] == '-')
                        {
                            throw new Exception();
                        }
                        else if (input[i] == '.' || input[i] == '\n' || input[i] == delimiter)
                        {

                            Int32.TryParse(temp, out incrementer);

                            Ifgreater(incrementer, out incrementer);
                            value += incrementer;
                            temp = "";
                        }
                        else if (char.IsDigit(input[i]))
                        {
                            temp += input[i];
                        }
                        else
                        {
                            if (counter == 2)
                            {
                                delimiter = input[i];
                            }
                        }

                    }

                    Int32.TryParse(temp, out incrementer);
                    Ifgreater(incrementer, out incrementer);
                    value += incrementer;
                    return value;
                }
            }

        }
        static int Ifgreater(int temp, out int input)
        {
            input = temp;
            if (input > 1000)
                input = 0;
            return input;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
